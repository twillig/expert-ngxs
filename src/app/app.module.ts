import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {EmitterComponent} from './emitter/emitter.component';
import {ReceiverComponent} from './receiver/receiver.component';
import {TransformerReceiverComponent} from './transformer-receiver/transformer-receiver.component';
import {NgxsModule, ReduxDevtoolsPlugin} from 'ngxs';
import {AsyncValueStore} from './store/async-value.store';
import {ItemsStore} from './store/items.store';
import {ThresholdStore} from './store/threshold.store';

@NgModule({
  declarations: [
    AppComponent,
    EmitterComponent,
    ReceiverComponent,
    TransformerReceiverComponent
  ],
  imports: [
    BrowserModule,
    NgxsModule.forRoot([
      AsyncValueStore,
      ItemsStore,
      ThresholdStore
    ], {
      plugins: [
        // `forRoot` is optional if you want to pass options
        ReduxDevtoolsPlugin.forRoot({
          disabled: false // Set to true for prod mode
        })
      ]
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
