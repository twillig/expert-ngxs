import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Select} from 'ngxs';
import {AsyncValueState} from '../store/async-value.store';
import {ThresholdState} from '../store/threshold.store';

@Component({
  selector: 'test-transformer-receiver',
  templateUrl: './transformer-receiver.component.html',
  styleUrls: ['./transformer-receiver.component.scss']
})
export class TransformerReceiverComponent {
  @Select('asyncValue')
  public asyncValue$: Observable<AsyncValueState>;
  @Select('threshold')
  public threshold$: Observable<ThresholdState>;
  @Select(state => state.items.map((item) => item.id))
  public ids$: Observable<number[]>;
  @Select(state => state.items.map((item) => item.name))
  public names$: Observable<string[]>;

  constructor() {}
}
