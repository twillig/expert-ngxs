import {Action, Mutation, Store} from 'ngxs';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';

export enum AsyncStateName {
  PENDING = 'PENDING',
  RESOLVED = 'RESOLVED',
  REJECTED = 'REJECTED'
}

export interface AsyncValueState {
  value: number;
  state: AsyncStateName;
}

export class RequestSetValueAction {
  constructor(public value: number) {}
}

export class ResponseSetValueAction {
  constructor(public value: number, public state: AsyncStateName) {}
}

@Store({
  defaults: {
    value: 0,
    state: AsyncStateName.RESOLVED
  }
})
export class AsyncValueStore {

  @Action(RequestSetValueAction)
  requestSetValue(state: AsyncValueState, { value }) {
    state.state = AsyncStateName.PENDING;
    return Observable.of(
      new ResponseSetValueAction(state.value + value, AsyncStateName.RESOLVED)
    ).delay(1000);
  }

  @Mutation(ResponseSetValueAction)
  responseSetValue(state: AsyncValueState, event: ResponseSetValueAction) {
    state.value = event.value;
    state.state = event.state;
  }
}
