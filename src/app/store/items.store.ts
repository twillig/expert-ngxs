import {Mutation, Store} from 'ngxs';

export type ItemsState = Array<{
  id: number;
  name: string;
}>;

export class PrefixNameAction {
  constructor(public prefix: string) {}
}

export class IncreaseIdsBy10Action {
  constructor() {}
}

export class DecreaseIdsBy10Action {
  constructor() {}
}

@Store({
  defaults: [
    {
      id: 234,
      name: 'ab'
    },
    {
      id: 345,
      name: 'bc'
    },
    {
      id: 456,
      name: 'cd'
    }
  ]
})
export class ItemsStore {

  @Mutation(PrefixNameAction)
  prefixNames(state: ItemsState, { prefix }) {
    for (const item of state) {
      const indexUnderscore = item.name.indexOf('_');

      if (indexUnderscore === -1) {
        if (prefix) {
          item.name = prefix + '_' + item.name;
        }
      } else {
        if (prefix) {
          item.name = prefix + '_' + item.name.substring(indexUnderscore + 1);
        } else {
          item.name = item.name.substring(indexUnderscore + 1);
        }
      }
    }
  }

  @Mutation([IncreaseIdsBy10Action, DecreaseIdsBy10Action])
  addToIds(state: ItemsState, event: IncreaseIdsBy10Action|DecreaseIdsBy10Action) {
    let amount = 0;

    if (event instanceof  IncreaseIdsBy10Action) {
      amount = 10;
    } else {
      amount = -10;
    }

    for (const item of state) {
      item.id += amount;
    }
  }
}
