import {Mutation, Store} from 'ngxs';

export interface ThresholdState {
 threshold: number;
}

export class SetThresholdAction {
  constructor(public threshold: number) {}
}

@Store({
  defaults: {
    threshold: 765
  }
})
export class ThresholdStore {

  @Mutation(SetThresholdAction)
  setThreshold(state: ThresholdState, { threshold }) {
    state.threshold = threshold;
  }
}
