import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Select} from 'ngxs';
import {AsyncValueState} from '../store/async-value.store';
import {ThresholdState} from '../store/threshold.store';
import {ItemsState} from '../store/items.store';

@Component({
  selector: 'test-receiver',
  templateUrl: './receiver.component.html',
  styleUrls: ['./receiver.component.scss']
})
export class ReceiverComponent {
  @Select('asyncValue')
  public asyncValue$: Observable<AsyncValueState>;
  @Select('threshold')
  public threshold$: Observable<ThresholdState>;
  @Select('items')
  public items$: Observable<ItemsState>;

  constructor() {}
}
