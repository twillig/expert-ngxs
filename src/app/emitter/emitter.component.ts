import {Component} from '@angular/core';
import {Ngxs} from 'ngxs';
import {DecreaseIdsBy10Action, IncreaseIdsBy10Action, PrefixNameAction} from '../store/items.store';
import {RequestSetValueAction} from '../store/async-value.store';

@Component({
  selector: 'test-emitter',
  templateUrl: './emitter.component.html',
  styleUrls: ['./emitter.component.scss']
})
export class EmitterComponent {
  constructor(private ngxs: Ngxs) {

  }

  onPrefixItems() {
    this.ngxs.dispatch(new PrefixNameAction('prefix'));
  }

  onRemovePrefixItems() {
    this.ngxs.dispatch(new PrefixNameAction(''));
  }

  onIncreaseIdBy10() {
    this.ngxs.dispatch(new IncreaseIdsBy10Action());
  }

  onDecreaseIdBy10() {
    this.ngxs.dispatch(new DecreaseIdsBy10Action());
  }

  onIncreaseAsyncBy10() {
    this.ngxs.dispatch(new RequestSetValueAction(10));
  }
}
